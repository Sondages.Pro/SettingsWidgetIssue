<?php
/**
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020 Denis Chenu <http://www.sondages.pro>
 * @license MIT
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class SettingsWidgetIssue extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'Issue in ext.SettingsWidget.SettingsWidget widget.';
    protected static $name = 'SettingsWidgetIssue';

    /* private boolean isDone */
    private $isDone = false;

    public function init()
    {
        /* Add settings update */
        $this->subscribe('beforeToolsMenuRender');
    }


    /**
     * see beforeToolsMenuRender event
     * @deprecated ? See https://bugs.limesurvey.org/view.php?id=15476
     * @return void
     */
    public function beforeToolsMenuRender()
    {
        $event = $this->getEvent();
        $surveyId = $event->get('surveyId');
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aMenuItem = array(
            'label' => $this->gT('Test widtg'),
            'iconClass' => 'fa fa-link',
            'href' => Yii::app()->createUrl(
                'admin/pluginhelper',
                array(
                    'sa' => 'sidebody',
                    'plugin' => get_class($this),
                    'method' => 'actionSettings',
                    'surveyId' => $surveyId
                )
            ),
        );
        $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
        $event->append('menuItems', array($menuItem));
    }

    /**
     * The main function
     * @param int $surveyId Survey id
     * @return void (redirect)
     */
    public function actionSaveSettings($surveyId)
    {
        if (empty(App()->getRequest()->getPost('save'.get_class($this)))) {
            throw new CHttpException(400);
        }
        $oSurvey=Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        $testSetting = App()->getRequest()->getPost('testSetting');
        $this->set('testSetting', $testSetting , 'Survey', $surveyId);
        $testSetting2 = App()->getRequest()->getPost('testSetting2');
        $this->set('testSetting2', $testSetting2 , 'Survey', $surveyId);

        if (App()->getRequest()->getPost('save'.get_class($this)) == 'redirect') {
            $redirectUrl = Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId));
            Yii::app()->getRequest()->redirect($redirectUrl);
        }
        $redirectUrl = Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId));
        Yii::app()->getRequest()->redirect($redirectUrl, true, 303);
    }
    /**
     * The main function
     * @param int $surveyId Survey id
     * @return string
     */
    public function actionSettings($surveyId)
    {
        $oSurvey=Survey::model()->findByPk($surveyId);
        $language = $oSurvey->language;
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        $aData['pluginClass'] = get_class($this);
        $aData['surveyId'] = $surveyId;
        $aSettings=array();

        $aSettings[$this->gT("Test setting")] = array(
            'testSetting' => array(
                'type'=>'text',
                'label'=>$this->gT("A test setting"),
                'current'=>$this->get('testSetting', 'Survey', $surveyId, ""),
            ),
            'testSetting2' => array(
                'type'=>'int',
                'htmlOptions'=>array(
                    'min'=>0,
                ),
                'label'=>$this->gT("A test setting 2"),
                'current'=>$this->get('testSetting2', 'Survey', $surveyId, ""),
            ),
        );
        $aData['aSettings']=$aSettings;
        $aData['form'] = array(
            'action' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSaveSettings','surveyId' => $surveyId)),
            'reset' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId)),
            'close' => Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId)),
        );
        $content = $this->renderPartial('admin.settings', $aData, true);
        return $content;
    }
}
